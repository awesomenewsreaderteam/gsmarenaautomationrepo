package com.gsmarena.automation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Date;

/**
 * Created by shweta on 7/3/2016.
 */
public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("F:/myGit/automation/gsmarenaAutomation/lib/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
        WebDriver webDriver = new ChromeDriver();
        // WebDriver webDriver = new FirefoxDriver();
        webDriver.get(Constants.URL);
        webDriver.manage().window().maximize();
        MobileData data = PageFactory.initElements(webDriver, MobileData.class);
        try {
            data.fetchData();
        } catch (Exception e) {
            webDriver.close();
            e.printStackTrace();
        }
        webDriver.close();
    }
}
