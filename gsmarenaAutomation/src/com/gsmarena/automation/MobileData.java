package com.gsmarena.automation;

import com.google.gson.Gson;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.io.File;
import java.io.PrintStream;
import java.util.*;

/**
 * Created by shweta on 7/3/2016.
 */
public class MobileData {


    @FindBy(className = "specs-phone-name-title")
    public WebElement mobileName;

    @FindBys({@FindBy(xpath = "//div[@id='specs-list']//table/tbody/tr/th")})
    public List<WebElement> specificationHeading;

    @FindBys({@FindBy(xpath = "//div[@id='specs-list']//table/tbody/tr/td")})
    public List<WebElement> specificationData;

    @FindBys({@FindBy(xpath = "//div[@id='specs-list']//table/tbody/tr")})
    public List<WebElement> specification;

    @FindBy(className = "link-network-detail")
    public WebElement expand;

    public void fetchData() throws Exception {
        Map<String, String> gadgetsDetails = new HashMap<>();
        Gson gson = new Gson();
        expand.click();
        System.out.println(mobileName.getText());
   /*     for (int i = 0; i < specification.size(); i++) {
            List<WebElement> specificationData = specification.get(i).findElements(By.xpath("./*//*"));
            for (int j = 0; j < specificationData.size(); j=j++) {
                WebElement specificationElementKey = specificationData.get(j);
                if (specificationElementKey.getTagName().equalsIgnoreCase("th")) {
                    System.out.println(specificationElementKey.getText() + "---->");
                }
                if (specificationElementKey.getTagName().equalsIgnoreCase("td")) {
                    System.out.println(specificationElementKey.getText());

                }
            }
            System.out.println("\n");
        }*/
        for (int i = 0; i < specification.size(); i++) {
            List<WebElement> specificationData = specification.get(i).findElements(By.xpath(".//td"));
            for (int j = 0; j < specificationData.size() / 2; j = j + 2) {
                String key = specificationData.get(j).getText();
                String value = specificationData.get(j + 1).getText();
                gadgetsDetails.put(key, value);
            }
        }
     /*   Iterator it = mymap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
           System.out.println(pair.getKey() + " = " + pair.getValue());
        }*/
        System.setOut(new PrintStream(new File("./JSONdata/" + mobileName.getText() + ".txt")));
        GadgetsDetails details = new GadgetsDetails(gadgetsDetails);
        System.out.println(gson.toJson(details));
    }
}
